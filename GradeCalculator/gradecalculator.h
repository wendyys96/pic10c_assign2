#ifndef GRADECALCULATOR_H
#define GRADECALCULATOR_H

#include <QMainWindow>
#include <QWidget>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>
#include <vector>



namespace Ui {
class GradeCalculator;
}

class GradeCalculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit GradeCalculator(QWidget *parent = 0);
    ~GradeCalculator();
public slots:
    void computeOverall();
    void updateOverall();


private:
    Ui::GradeCalculator *ui;
    std::vector<QSlider*> slider;
    std::vector<QSpinBox*> spinbox;
    std::vector<QLabel*>label;
    int calculatedScore = 0;
};

#endif // GRADECALCULATOR_H
