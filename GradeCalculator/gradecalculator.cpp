#include "gradecalculator.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GradeCalculator)
{
    ui->setupUi(this);
    slider.push_back(ui->horizontalSlider);
    slider.push_back(ui->horizontalSlider_2);
    slider.push_back(ui->horizontalSlider_3);
    slider.push_back(ui->horizontalSlider_4);
    slider.push_back(ui->horizontalSlider_5);
    slider.push_back(ui->horizontalSlider_6);
    slider.push_back(ui->horizontalSlider_7);
    slider.push_back(ui->horizontalSlider_8);
    slider.push_back(ui->horizontalSlider_9);
    slider.push_back(ui->horizontalSlider_10);
    slider.push_back(ui->horizontalSlider_11);
    slider.push_back(ui->horizontalSlider_12);

    spinbox.push_back(ui->spinBox);
    spinbox.push_back(ui->spinBox_2);
    spinbox.push_back(ui->spinBox_3);
    spinbox.push_back(ui->spinBox_4);
    spinbox.push_back(ui->spinBox_5);
    spinbox.push_back(ui->spinBox_6);
    spinbox.push_back(ui->spinBox_7);
    spinbox.push_back(ui->spinBox_8);
    spinbox.push_back(ui->spinBox_9);
    spinbox.push_back(ui->spinBox_10);
    spinbox.push_back(ui->spinBox_11);
    spinbox.push_back(ui->spinBox_12);

    label.push_back(ui->label_2);
    label.push_back(ui->label_3);
    label.push_back(ui->label_4);
    label.push_back(ui->label_5);
    label.push_back(ui->label_6);
    label.push_back(ui->label_7);
    label.push_back(ui->label_8);
    label.push_back(ui->label_9);
    label.push_back(ui->label_10);
    label.push_back(ui->label_11);
    label.push_back(ui->label_12);
    label.push_back(ui->label_15);


   QObject::connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(computeOverall()));

   ui->comboBox->setCurrentIndex(1);

   QObject::connect(ui->radioButton, SIGNAL(clicked(bool)), this, SLOT(computeOverall()));
   QObject::connect(ui->radioButton_2, SIGNAL(clicked(bool)), this, SLOT(computeOverall()));

   QObject::connect(ui->radioButton, SIGNAL(checked(bool)), this, SLOT(computeOverall()));
   QObject::connect(ui->radioButton_2, SIGNAL(checked(bool)), this, SLOT(computeOverall()));

    for(size_t i = 0; i < slider.size(); ++i){
        slider[i]->setRange(0, 100);
        spinbox[i]->setRange(0, 100);
        QObject::connect(slider[i], SIGNAL(valueChanged(int)), spinbox[i], SLOT(setValue(int)));
        QObject::connect(spinbox[i], SIGNAL(valueChanged(int)), slider[i], SLOT(setValue(int)));
        QObject::connect(spinbox[i], SIGNAL(valueChanged(int)), this, SLOT(computeOverall()));
    }

    QObject::connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(updateOverall()));

}


void GradeCalculator::computeOverall() {
    int homework = 0;
    int numHomework;
    int averageHomework;
    int midterm1 = slider[slider.size() - 4]->value();
    int midterm2 = slider[slider.size() - 3]->value();
    int final = slider[slider.size() - 2]->value();

    int percentageHomework;
    int percentageMidterm1;
    int percentageMidterm2;
    int percentageFinal;
    if(ui->comboBox->currentIndex() == 0){
        for(size_t i = 0; i < slider.size() - 4; ++i){
            homework += slider[i]->value();
        }
        for (size_t i = 0; i < slider.size() - 1; ++i){
            slider[i]->show();
            spinbox[i]->show();
            label[i]->show();
        }
        slider[slider.size() - 1]->hide();
        spinbox[spinbox.size() - 1]->hide();
        label[label.size() - 1]->hide();

        numHomework = 8;
        averageHomework = homework / numHomework;

        if (ui->radioButton->isChecked()){
            percentageHomework = 25;
            percentageMidterm1 = 20;
            percentageMidterm2 = 20;
            percentageFinal = 35;
            if (midterm1 == 100 && midterm2 == 100 && averageHomework == 100 && final == 100) calculatedScore = 100;
            calculatedScore = (percentageHomework * averageHomework + percentageMidterm1 * midterm1 + percentageMidterm2 * midterm2 + percentageFinal * final) / 100;
        }
        else if (ui->radioButton_2->isChecked()){
            percentageHomework = 25;
            percentageMidterm1 = 30;
            percentageFinal = 44;
            int higherMidterm;
            if (midterm1 > midterm2) higherMidterm = midterm1;
            else higherMidterm = midterm2;
            if (higherMidterm == 100 && averageHomework == 100 && final == 100) calculatedScore = 100;
            else calculatedScore = (percentageHomework * averageHomework + percentageMidterm1 * higherMidterm + percentageFinal * final) / 100;

        }
    }
    else{
        for(size_t i = 0; i < 3; ++i){
            slider[i]->show();
            spinbox[i]->show();
            label[i]->show();
        }
        for(size_t i = 3; i < 8; ++i){
            slider[i]->hide();
            spinbox[i]->hide();
            label[i]->hide();
        }
        slider[label.size() - 4]->show();
        spinbox[label.size() - 4]->show();
        label[label.size() - 4]->show();
        slider[label.size() - 3]->hide();
        spinbox[label.size() - 3]->hide();
        label[label.size() - 3]->hide();
        slider[label.size() - 2]->show();
        spinbox[label.size() - 2]->show();
        label[label.size() - 2]->show();

        slider[slider.size() - 1]->show();
        spinbox[spinbox.size() - 1]->show();
        label[label.size() - 1]->show();

        for(size_t i = 0; i < 3; ++i){
            homework += slider[i]->value();
        }
        numHomework = 3;
        averageHomework = homework / numHomework;
        percentageHomework = 15;
        int percentageFinalProject = 35;
        int finalProject = slider[slider.size() - 1]->value();
        if(ui->radioButton->isChecked()){
            percentageMidterm1 = 25;
            percentageFinal = 30;
            calculatedScore = (percentageHomework * averageHomework + percentageMidterm1 * midterm1 + percentageFinalProject * finalProject + percentageFinal * final) / 100;
        }
        else if(ui->radioButton_2->isChecked()){
            percentageFinal = 50;
            calculatedScore = (percentageHomework * averageHomework + percentageFinal * final + percentageFinalProject * finalProject) / 100;

        }
    }

}


void GradeCalculator::updateOverall(){
    //double score = 31.4;
    double score = static_cast<double>(calculatedScore);
    ui->label_14->setText(QString::number(score));

    return;
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}
