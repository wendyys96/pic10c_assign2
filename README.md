# Assignment 2: Grade Calculator

The purpose of this assignment is to practice using Qt.

## Class 1: PIC10B: Intermediate Programming
Scheme 1: 25% homework, 20% midterm 1, 20% midterm 2, 35% final  
  
Scheme 2: 25% homework, 30% higher midterm, 44%final

## Class 2: PIC10C: Advanced Programming
Scheme 1: 15% homework, 25% midterm, 35% final project, 30% final  
  
Scheme 2: 15% homework, 50% final project, 35% final


|**Files**|
|:------------:|
| [`README.md`][read-me] |
| [`GradeCalculator.pro`][pro]|
| [`gradecalculator.h`][header-file] |
| [`gradecalculator.cpp`][gradecalculator] |
| [`gradecalculator.ui`][ui] |
| [`main.cpp`][main] |


[read-me]: README.md
[pro]: GradeCalculator/GradeCalculator.pro
[header-file]: GradeCalculator/gradecalculator.h
[gradecalculator]: GradeCalculator/gradecalculator.cpp
[ui]: GradeCalculator/gradecalculator.ui
[main]: GradeCalculator/main.cpp